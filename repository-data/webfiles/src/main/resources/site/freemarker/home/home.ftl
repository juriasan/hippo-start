<#assign hst=JspTaglibs["http://www.hippoecm.org/jsp/hst/core"] >
<html>
    <head>
    </head>
    <body>
        <h1>Hello World </h1>
            <@hst.actionURL var="actionLink"/>
            <form action="${actionLink}" method="post">
                <!-- form fields here -->
                <label for="query-input">Enter query:</label>
                <br />
                <br />
                <input type="text" id="query-input" name="query" />
                <br />
                <br />
                <button type="submit">Submit!</button>
            </form>
        <#if result??>
            <label>Total hits: ${result.totalHits}</label>
            <br/>
            <br />
            <#list result.hits as image>
                ${image.likes}
                <br/>
                <br/>
            </#list>
        </#if>
         <#--<#if document??>-->
            <#--<h1>${document.id?html}</h1>-->
            <#--<div>-->
              <#--<@hst.html hippohtml=document.name />-->
            <#--</div>-->
         <#--<#else>-->
              <#--<h1>Goodbye? cruel world</h1>-->
         <#--</#if>-->
    </body>
</html>