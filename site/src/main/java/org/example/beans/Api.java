package org.example.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;

@HippoEssentialsGenerated(internalName = "myhippoproject:api")
@Node(jcrType = "myhippoproject:api")
public class Api extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "myhippoproject:AuthKey")
    public String getAuthKey() {
        return getProperty("myhippoproject:AuthKey");
    }

    @HippoEssentialsGenerated(internalName = "myhippoproject:URL")
    public String getURL() {
        return getProperty("myhippoproject:URL");
    }
}
