package org.example.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;

@HippoEssentialsGenerated(internalName = "myhippoproject:image")
@Node(jcrType = "myhippoproject:image")
public class Image extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "myhippoproject:largeImageUrl")
    public String getLargeImageUrl() {
        return getProperty("myhippoproject:largeImageUrl");
    }

    @HippoEssentialsGenerated(internalName = "myhippoproject:webFormatHeight")
    public Long getWebFormatHeight() {
        return getProperty("myhippoproject:webFormatHeight");
    }

    @HippoEssentialsGenerated(internalName = "myhippoproject:webFormatWidth")
    public Long getWebFormatWidth() {
        return getProperty("myhippoproject:webFormatWidth");
    }

    @HippoEssentialsGenerated(internalName = "myhippoproject:likes")
    public Long getLikes() {
        return getProperty("myhippoproject:likes");
    }
}
