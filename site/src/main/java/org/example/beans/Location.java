package org.example.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;

@HippoEssentialsGenerated(internalName = "myhippoproject:location")
@Node(jcrType = "myhippoproject:location")
public class Location extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "myhippoproject:id")
    public String getId() {
        return getProperty("myhippoproject:id");
    }

    @HippoEssentialsGenerated(internalName = "myhippoproject:name")
    public String getName() {
        return getProperty("myhippoproject:name");
    }
}
