package org.example.beans;

import java.util.List;

public class PixabayResponse {

    private int totalHits;
    private List<Image> hits;

    public int getTotalHits() {
        return totalHits;
    }

    public List<Image> getHits() {
        return hits;
    }
}

class Img {

    private String largeImageURL;
    private long webformatHeight;
    private long webformatWidth;
    private long likes;
    private long imageWidth;
    private String id;
    private long views;
    private String pageURL;

    public String getLargeImageURL() {
        return largeImageURL;
    }

    public long getWebFormatHeight() {
        return webformatHeight;
    }

    public long getWebformatWidth() {
        return webformatWidth;
    }

    public long getLikes() {
        return likes;
    }

    public long getImageWidth() {
        return imageWidth;
    }

    public String getId() {
        return this.id;
    }

    public long getViews() {
        return this.views;
    }

    public String getPageURL() {
        return pageURL;
    }
}
