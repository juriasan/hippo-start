package org.example.components;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.lang.StringUtils;
import org.example.beans.Api;
import org.example.beans.BaseDocument;
import org.example.beans.PixabayResponse;
import org.hippoecm.hst.component.support.bean.BaseHstComponent;
import org.hippoecm.hst.component.support.forms.FormField;
import org.hippoecm.hst.component.support.forms.FormMap;
import org.hippoecm.hst.component.support.forms.FormUtils;
import org.hippoecm.hst.content.beans.ObjectBeanPersistenceException;
import org.hippoecm.hst.content.beans.manager.workflow.BaseWorkflowCallbackHandler;
import org.hippoecm.hst.content.beans.manager.workflow.WorkflowPersistenceManager;
import org.hippoecm.hst.content.beans.query.HstQuery;
import org.hippoecm.hst.content.beans.query.HstQueryResult;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoBeanIterator;
import org.hippoecm.hst.core.component.HstComponentException;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.hippoecm.hst.util.SearchInputParsingUtils;
import org.onehippo.repository.documentworkflow.DocumentWorkflow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;
import java.util.Optional;

public class SimpleComponent extends BaseHstComponent {

    public static final Logger log = LoggerFactory.getLogger(SimpleComponent.class);

    private void createBean(HstRequest request) {
        WorkflowPersistenceManager wpm = null;
        HstRequestContext requestContext = request.getRequestContext();
        try {
            wpm = getWorkflowPersistenceManager(requestContext.getSession());
            wpm.setWorkflowCallbackHandler(
                    new BaseWorkflowCallbackHandler<DocumentWorkflow>() {
                        public void processWorkflow(
                                DocumentWorkflow wf) throws Exception {
                            wf.requestPublication();
                        }
                    });

            // it is not important where we store comments. WE just use
            // some timestamp path below our project content
            String siteCanonicalBasePath =
                    request.getRequestContext().getResolvedMount()
                            .getMount().getCanonicalContentPath();

            wpm.createAndReturn(siteCanonicalBasePath,
                    "myhippoproject:location",
                    "location1", true);

        } catch (Exception e) {
            log.warn("Failed to create a comment: ", e);

            if (wpm != null) {
                try {
                    wpm.refresh();
                } catch (ObjectBeanPersistenceException e1) {
                    log.warn("Failed to refresh: ", e);
                }
            }
        }
    }

    @Override
    public void doBeforeRender(HstRequest request, HstResponse response) throws HstComponentException {
       // createBean(request);

        FormMap map = new FormMap();
        FormUtils.populate(request, map);

        Optional.ofNullable(map.getField("resultJson"))
                .map(v -> v.getValue())
                .map(val -> new GsonBuilder().create().fromJson(val, PixabayResponse.class))
                .ifPresent(resp -> request.setAttribute("result", resp));

//        try {
//
//            HstRequestContext context = request.getRequestContext();
//            HippoBean scope = context.getSiteContentBaseBean();
//            HstQuery hstQuery =
//                    context.getQueryManager().createQuery(scope,
//                            BaseDocument.class, true);
//            // always set a limit! The limit is normally the pageSize,
//            // for example 10
//            hstQuery.setLimit(10);
//            HippoBeanIterator result = hstQuery.execute().getHippoBeans();
//            while (result.hasNext()) {
//                HippoBean b = result.nextHippoBean();
//                int i = 1;
//            }
//            int i = 1;
//            // the offset : Assume requested page is 3. Then the offset is 20
//            //hstQuery.setOffset(10 * (currentPage - 1));
//            // we assume ordering on property "mynamespace:date" descending
//            //hstQuery.addOrderByDescending("mynamespace:date");
//
//            // parse a free text query to remove invalid chars. The argument
//            // 'false' means no wildcards allowed
//            //String parsedQuery = SearchInputParsingUtils.parse(parsedQuery, false);
//        } catch (Exception ex) {
//
//        }
    }

    @Override
    public void doAction(HstRequest request, HstResponse response) throws HstComponentException {

        final HstRequestContext ctx = request.getRequestContext();

        super.doAction(request, response);
        FormMap fmap = new FormMap(request, new String[]{"query"});
        FormUtils.persistFormMap(request, response, fmap, null);

        Map<String, String[]> map = request.getParameterMap();
        Optional<String> query = Optional.ofNullable(map.getOrDefault("query", null))
                .map(values -> {
                    if (values.length > 0) {
                        return values[0];
                    }
                    return null;
                });

        Api api = (Api) ctx.getContentBean();

        if (api != null && query.isPresent() && !StringUtils.isBlank(query.get()) && !StringUtils.isBlank(api.getAuthKey())) {

            Optional<HttpURLConnection> connection = Optional.empty();
            try {
                String strurl = api.getURL() + "?key=" + api.getAuthKey() + "&q=" + query.get();
                URL url = new URL(strurl);

                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");
                con.setRequestProperty("Content-Type", "application/json");
                con.setConnectTimeout(5000);
                con.setReadTimeout(5000);

                connection = Optional.of(con);
            } catch (IOException ex) {
                //return
            }
            connection.ifPresent(con -> {
                try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
                    String inputLine;
                    StringBuffer content = new StringBuffer();
                    while ((inputLine = in.readLine()) != null) {
                        content.append(inputLine);
                    }
                    FormField f = new FormField("resultJson");
                    f.setValueList(new ArrayList<String>() {{
                        this.add(content.toString());
                    }});

                    fmap.addFormField(f);
                    FormUtils.persistFormMap(request, response, fmap, null);

                } catch (IOException ex) {
                    //return
                }
            });
        }
    }
}
